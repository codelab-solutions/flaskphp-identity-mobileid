

import static org.junit.Assume.assumeTrue;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.time.LocalDate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.ProcessingException;

import ee.sk.mid.MidAuthenticationHashToSign;
import ee.sk.mid.MidClient;
import ee.sk.mid.MidDisplayTextFormat;
import ee.sk.mid.MidLanguage;
import ee.sk.mid.exception.MidInternalErrorException;
import ee.sk.mid.exception.MidUnauthorizedException;
import ee.sk.mid.rest.dao.request.MidAuthenticationRequest;
import org.junit.Test;

public class MobileIdSSL_IT {

    private MidClient client;

    public static final LocalDate LIVE_SERVER_CERT_EXPIRATION_DATE = LocalDate.of(2021, 3, 25);
    public static final String LIVE_SERVER_CERT = "-----BEGIN CERTIFICATE-----\n" +
            "MIIGezCCBWOgAwIBAgIQBs+E+B8gYnf1I31IIanXXjANBgkqhkiG9w0BAQsFADBN\n" +
            "MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMScwJQYDVQQDEx5E\n" +
            "aWdpQ2VydCBTSEEyIFNlY3VyZSBTZXJ2ZXIgQ0EwHhcNMTkwMzIxMDAwMDAwWhcN\n" +
            "MjEwMzI1MTIwMDAwWjBQMQswCQYDVQQGEwJFRTEQMA4GA1UEBxMHVGFsbGlubjEb\n" +
            "MBkGA1UEChMSU0sgSUQgU29sdXRpb25zIEFTMRIwEAYDVQQDEwltaWQuc2suZWUw\n" +
            "ggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDE0RI6DQ7wN5hKhlhCSN7Z\n" +
            "x68hIfGG54XktQLbnvSeJSHZqqSJTCYSkMPQ1cSTMolviHdOWl7qUzX7OCoseV+g\n" +
            "okvgig83amfPR25Qdt3vzvCLT0gj4GojKIYtSSRqU9lsXliib0lNypdBoPvUKicT\n" +
            "1WWHz8pnUv7ZK/iu9190hjGaUxbqmJWyFSjh8Olowr1I2mGCWf7ymAX5Lqnk5Gxi\n" +
            "J9r79e5JTPx0dOaIgC+Fo3ZrH1xSdpXb3ycSMWwMsYoLN1D4J8fIOBk4GDB1UwBJ\n" +
            "QMu3F90sXjbaJrwgHeHP6LNxKY3BYOe3uVy+zXiNcmIirr6x4oS0lL90QFSGq/R1\n" +
            "AgMBAAGjggNSMIIDTjAfBgNVHSMEGDAWgBQPgGEcgjFh1S8o541GOLQs4cbZ4jAd\n" +
            "BgNVHQ4EFgQU2+x3/zzTZeraNrpJb/B6SL1r4d4wFAYDVR0RBA0wC4IJbWlkLnNr\n" +
            "LmVlMA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUH\n" +
            "AwIwawYDVR0fBGQwYjAvoC2gK4YpaHR0cDovL2NybDMuZGlnaWNlcnQuY29tL3Nz\n" +
            "Y2Etc2hhMi1nNi5jcmwwL6AtoCuGKWh0dHA6Ly9jcmw0LmRpZ2ljZXJ0LmNvbS9z\n" +
            "c2NhLXNoYTItZzYuY3JsMEwGA1UdIARFMEMwNwYJYIZIAYb9bAEBMCowKAYIKwYB\n" +
            "BQUHAgEWHGh0dHBzOi8vd3d3LmRpZ2ljZXJ0LmNvbS9DUFMwCAYGZ4EMAQICMHwG\n" +
            "CCsGAQUFBwEBBHAwbjAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuZGlnaWNlcnQu\n" +
            "Y29tMEYGCCsGAQUFBzAChjpodHRwOi8vY2FjZXJ0cy5kaWdpY2VydC5jb20vRGln\n" +
            "aUNlcnRTSEEyU2VjdXJlU2VydmVyQ0EuY3J0MAwGA1UdEwEB/wQCMAAwggF+Bgor\n" +
            "BgEEAdZ5AgQCBIIBbgSCAWoBaAB2AO5Lvbd1zmC64UJpH6vhnmajD35fsHLYgwDE\n" +
            "e4l6qP3LAAABaaDXZ0QAAAQDAEcwRQIgN7q4F8UJyQOT8OsG8h96BZHRdMUk4Aly\n" +
            "G7tztptFBW8CIQDF7tr5je9pxFzlczVwdq6LzlI9cnSnloCdgJ0E2/P5sQB2AId1\n" +
            "v+dZfPiMQ5lfvfNu/1aNR1Y2/0q1YMG06v9eoIMPAAABaaDXaIIAAAQDAEcwRQIg\n" +
            "RSfaNfCLY/0tvCIw+oVusNddo4lSa++xCIqMvjnkZ6YCIQCv+UoMOs9kCd5yZbay\n" +
            "jXCbVuiNrWvDijYGGF2lfPWpDwB2AESUZS6w7s6vxEAH2Kj+KMDa5oK+2MsxtT/T\n" +
            "M5a1toGoAAABaaDXZwEAAAQDAEcwRQIgL3CaRptYqf/5EPebOO/QzWn9xJh2fbeu\n" +
            "BQaYCYNtECwCIQCBnj61xJxy361r1qAI5Y7EZIUWt8Z/9vxztACxf/mPMDANBgkq\n" +
            "hkiG9w0BAQsFAAOCAQEAPqjpkav+c7bZSMFRwTB3+t68UD0zG7JFRWblxqi4QcG8\n" +
            "bTDoXfrZTp8nC0FQa56SbQVrFlkP6306+O9Itc09049J3qBZ3YDXNy4aetsL8LMa\n" +
            "VqF8mZadv2BQz6mCw56XLgKJVhKRA6QVHRgsocx9Ujp9NZsdP7JxhFIHXUAu6CHk\n" +
            "SYZoUeXL3/mwbr/ul6JvF5cQ8uyxVz7uw5narW9+I8hlzbAXLzL126MyAbQ+v45E\n" +
            "2goHz9848QEGlu6AtlCvcmp8VqO+BH6e4e4a+ihUaXy1ykCgCw4Nq+3VVARdVv6+\n" +
            "s/OHdPfZDLVzkZJA4Vl/GqmJpFAUF+FtG/oFT5gmRw==\n" +
            "-----END CERTIFICATE-----\n";

    public static final LocalDate DEMO_SERVER_CERT_EXPIRATION_DATE = LocalDate.of(2021, 1, 15);
    public static final String DEMO_SERVER_CERT = "-----BEGIN CERTIFICATE-----\n" +
            "MIIGCTCCBPGgAwIBAgIQB5CCfJUfCEruWfwaDQQ8ojANBgkqhkiG9w0BAQsFADBN\n" +
            "MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMScwJQYDVQQDEx5E\n" +
            "aWdpQ2VydCBTSEEyIFNlY3VyZSBTZXJ2ZXIgQ0EwHhcNMjAwMTA0MDAwMDAwWhcN\n" +
            "MjEwMTE1MTIwMDAwWjBVMQswCQYDVQQGEwJFRTEQMA4GA1UEBxMHVGFsbGlubjEb\n" +
            "MBkGA1UEChMSU0sgSUQgU29sdXRpb25zIEFTMRcwFQYDVQQDEw50c3AuZGVtby5z\n" +
            "ay5lZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMEXbF6n3XKvkLKy\n" +
            "EcnYoPBTvHaqjWIXnguu/aLiEC17ZuELrf4YkwXcQ6mTK6t1H21p7bluWDuhGzy1\n" +
            "pcf4zSPD7SYBYFDQJZUHEC54TPDRkZkm8vVYrtQ3s/I7VcDF54Gp2jy5QrZ/KKtx\n" +
            "qT1L3J7VNjNcjHp1qg5nGoNMfMHajaZITVmXUV7MdcVwgXunjK3I4R48TxkfevEO\n" +
            "QkeJMW4Nj+tuqd/aj3iPxBRC5N9QnwsUFh+GlTvWO7JdN4RgUvrpzYCXITdcR9fb\n" +
            "+GN62LwUioNar+ixzbx5x4+aKeiZch57mQnnccuAlaJZ50/XB38Pil5aJvSb1cqN\n" +
            "zhESRGsCAwEAAaOCAtswggLXMB8GA1UdIwQYMBaAFA+AYRyCMWHVLyjnjUY4tCzh\n" +
            "xtniMB0GA1UdDgQWBBT6vCJtkTvBJNfLz37w57NiN/s+ATAZBgNVHREEEjAQgg50\n" +
            "c3AuZGVtby5zay5lZTAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUH\n" +
            "AwEGCCsGAQUFBwMCMGsGA1UdHwRkMGIwL6AtoCuGKWh0dHA6Ly9jcmwzLmRpZ2lj\n" +
            "ZXJ0LmNvbS9zc2NhLXNoYTItZzYuY3JsMC+gLaArhilodHRwOi8vY3JsNC5kaWdp\n" +
            "Y2VydC5jb20vc3NjYS1zaGEyLWc2LmNybDBMBgNVHSAERTBDMDcGCWCGSAGG/WwB\n" +
            "ATAqMCgGCCsGAQUFBwIBFhxodHRwczovL3d3dy5kaWdpY2VydC5jb20vQ1BTMAgG\n" +
            "BmeBDAECAjB8BggrBgEFBQcBAQRwMG4wJAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3Nw\n" +
            "LmRpZ2ljZXJ0LmNvbTBGBggrBgEFBQcwAoY6aHR0cDovL2NhY2VydHMuZGlnaWNl\n" +
            "cnQuY29tL0RpZ2lDZXJ0U0hBMlNlY3VyZVNlcnZlckNBLmNydDAMBgNVHRMBAf8E\n" +
            "AjAAMIIBAgYKKwYBBAHWeQIEAgSB8wSB8ADuAHUAu9nfvB+KcbWTlCOXqpJ7RzhX\n" +
            "lQqrUugakJZkNo4e0YUAAAFvcP7EQwAABAMARjBEAiA2fFBYg7BrD8fvSMUPdSIk\n" +
            "CcASBgvqn6ySm//nyYT7jgIgDl3+FTpQJyLXTqzurjna9AnbNZkGiaoxEdCL6iBW\n" +
            "s2YAdQBElGUusO7Or8RAB9io/ijA2uaCvtjLMbU/0zOWtbaBqAAAAW9w/sPdAAAE\n" +
            "AwBGMEQCIBXEX2dbwSUQJfo6pP0Uf/YLVC200QfIdO+1oESfxLIMAiAWGLVR4MTe\n" +
            "H2+iOK+Hndbo9LkDdMibWTyIIByLCyHKhzANBgkqhkiG9w0BAQsFAAOCAQEAWOWm\n" +
            "sTfs3TSDd04c3GvB0b+x5xu34SYG8OCYfpTbdU5X8+7mk3+XR9yqBZpN/WSBBk0f\n" +
            "Vx+ukpON3z1v/TOMMHSOykxxw3yQsNB+NZ/a6d7ns4OBsRY4/TLu1DI1Ey7jkE0m\n" +
            "erqbzCAgx3nrHwo49bUNLtkgnUHoKNoLYreLQvAjW7PeiPmT/xkvz7MC3jE5P/hA\n" +
            "rZ5xvV/ZxpiRVDuDT0G+uCoIuBjY4HpvMgOJdsqxKtK1NI1dodPyjxVmMdjG6+1X\n" +
            "Kd5GtbPeaLRx1Kpe/NkfGAruW4TCvuUm2G1zHs71ePmYSPJjE6FDOnWWqjtQIgXg\n" +
            "OauLK5GGqW/2PvCWXA==\n" +
            "-----END CERTIFICATE-----";

}
