
import java.util.concurrent.TimeUnit;

import java.io.*;
//import java.io.IOException;
import java.io.File;

import org.apache.commons.io.FileUtils;


import ee.sk.mid.*;
import ee.sk.mid.MidClient;
import ee.sk.mid.rest.*;
import ee.sk.mid.rest.dao.*;
import ee.sk.mid.rest.dao.request.MidCertificateRequest;
import ee.sk.mid.rest.dao.response.MidCertificateChoiceResponse;
import ee.sk.mid.rest.dao.MidSessionStatus;
import ee.sk.mid.rest.dao.request.MidAuthenticationRequest;
import ee.sk.mid.rest.dao.request.MidSessionStatusRequest;
import ee.sk.mid.rest.dao.request.MidSignatureRequest;
import ee.sk.mid.rest.dao.response.MidAuthenticationResponse;
import ee.sk.mid.rest.dao.response.MidSignatureResponse;
import org.digidoc4j.*;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;


import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.FileUtils;

import java.util.Base64;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class Sign {

    static String URL = "";
    static String UUID = "";
    static String Name = "";
    static String IDCode = "";
    static String PhoneNo = "";
    static String DisplayText = "";
    static String Filename = "";
    static String Environment = "";
    static String SignerInfo = "";
    static String TmpFolder = "";


    public static void main(String[] args) throws InterruptedException, IOException, KeyStoreException, NoSuchAlgorithmException, CertificateException {

        // Args 0 - URL
        // Args 1 - UUID
        // Args 2 - Name
        // Args 3 - ID code
        // Args 4 - country code
        // Args 5 - TEMP file name
        // Args 6 - Environment
        // Args 7 - SignerInfo
        // Args 8 - TmpFolder

        for (int i = 0; i < args.length; i++) {
            switch (i) {
                case 0:
                    setUrl(args[i]);
                    break;
                case 1:
                    setUUID(args[i]);
                    break;
                case 2:
                    setName(args[i]);
                    break;
                case 3:
                    setIDCode(args[i]);
                    break;
                case 4:
                    setPhoneNo(args[i]);
                    break;
                case 5:
                    setDisplayText(args[i]);
                    break;
                case 6:
                    setFilename(args[i]);
                    break;
                case 7:
                    setEnvironment(args[i]);
                    break;
                case 8:
                    setSignerInfo(args[i]);
                    break;
                case 9:
                    setTmpFolder(args[i]);
                    break;
            }
        }


        try {
            PrintStream out = new PrintStream(new FileOutputStream(getFilename()));
            System.setOut(out);
            System.setErr(out);
        } catch (FileNotFoundException ex) {
            System.out.println("[error] " + ex);
        }


        System.out.println("[Starting signing process] ");

        String trustStoreFile = "";
        if (getEnvironment().equals("TEST")) {
            trustStoreFile = "./resources/truststore-test.jks";
        } else {
            trustStoreFile = "./resources/truststore.jks";
        }
        InputStream is = MobileIdSSL_IT.class.getResourceAsStream(trustStoreFile);

        KeyStore trustStore = KeyStore.getInstance("JKS");
        trustStore.load(is, "changeit".toCharArray());

        MidClient client = MidClient.newBuilder()
                .withRelyingPartyUUID(getUUID())
                .withRelyingPartyName(getName())
                .withHostUrl(getUrl())
                .withTrustStore(trustStore)
                .build();

        MidCertificateRequest request = MidCertificateRequest.newBuilder()
                .withPhoneNumber(getPhoneNo())
                .withNationalIdentityNumber(getIDCode())
                .build();

        MidCertificateChoiceResponse response = client.getMobileIdConnector().getCertificate(request);

        X509Certificate certificate = client.createMobileIdCertificate(response);

        Container container;

        String fileNameWithExt = new File(getFilename()).getName();
        String fileName = FilenameUtils.removeExtension(fileNameWithExt);

        if (getEnvironment().equals("TEST")) {
            Configuration configuration = new Configuration(Configuration.Mode.TEST);
            configuration.setTslLocation("https://open-eid.github.io/test-TL/tl-mp-test-EE.xml");

            container = ContainerBuilder.
                    aContainer("ASICE").
                    withConfiguration(configuration).
                    fromExistingFile(getTmpFolder() + "/" + fileName + ".asice").
                    build();
        } else {

            container = ContainerBuilder.
                    aContainer("ASICE").
                    fromExistingFile(getTmpFolder() + "/" + fileName + ".asice").
                    build();
        }

        // Decode Signer info
        byte[] decoded = Base64.getDecoder().decode(getSignerInfo());

        String decodedArray = new String(decoded);

        JSONObject jsonObject = new JSONObject(decodedArray);

        SignatureBuilder builder = SignatureBuilder.
                aSignature(container).
                withCity((String) jsonObject.get("city")).
                withStateOrProvince((String) jsonObject.get("state")).
                withPostalCode((String) jsonObject.get("postalcode")).
                withCountry((String) jsonObject.get("country")).
                withRoles((String) jsonObject.get("role")).
                withSigningCertificate(certificate).
                withSignatureDigestAlgorithm(DigestAlgorithm.SHA256);

        //Get the data to be signed by the user
        DataToSign dataToSign = builder.
                buildDataToSign();

        //Data to sign contains the signature dataset with digest of file that should be signed
        byte[] signatureToSign = dataToSign.getDataToSign();

        MidHashToSign hashToSign = MidHashToSign.newBuilder()
                .withDataToHash(signatureToSign)
                .withHashType(MidHashType.SHA256)
                .build();

        // to display the verification code
        String verificationCode = hashToSign.calculateVerificationCode();

        System.out.println("[verification_code] " + verificationCode);

        // Decode Display text
        byte[] decoded2 = Base64.getDecoder().decode(getDisplaytext());

        String displayText = new String(decoded2);

        MidSignatureRequest request2 = MidSignatureRequest.newBuilder()
                .withPhoneNumber(getPhoneNo())
                .withNationalIdentityNumber(getIDCode())
                .withHashToSign(hashToSign)
                .withLanguage(MidLanguage.ENG)
                .withDisplayText(displayText)
                .withDisplayTextFormat(MidDisplayTextFormat.GSM7)
                .build();

        MidSignatureResponse response2 = client.getMobileIdConnector().sign(request2);


        MidSessionStatus sessionStatus = client.getSessionStatusPoller().fetchFinalSessionStatus(response2.getSessionID(),
                "/signature/session/{sessionId}");

        MidSignature signature = client.createMobileIdSignature(sessionStatus);

        //Sign the signature dataset with digest of file
        byte[] signatureValue = signature.getValue();

        //Finalize the signature with OCSP response and timestamp (or timemark)
        Signature signature2 = dataToSign.finalize(signatureValue);

        //Add signature to the container
        container.addSignature(signature2);

        //Save the container as a .bdoc file
        container.saveAsFile(getTmpFolder() + "/" + fileName + ".asice");

        System.out.println("[success]");

    }

    public static void setFilename(String filename) {
        Filename = filename;
    }

    public static String getFilename() {
        return Filename;
    }

    public static void setUrl(String url) {
        URL = url;
    }

    public static String getUrl() {
        return URL;
    }

    public static void setUUID(String uuid) {
        UUID = uuid;
    }

    public static String getUUID() {
        return UUID;
    }

    public static void setName(String name) {
        Name = name;
    }

    public static String getName() {
        return Name;
    }

    public static void setIDCode(String idcode) {
        IDCode = idcode;
    }

    public static String getIDCode() {
        return IDCode;
    }

    public static void setPhoneNo(String phoneno) {
        PhoneNo = phoneno;
    }

    public static String getPhoneNo() {
        return PhoneNo;
    }

    public static void setDisplayText(String displayText) {
        DisplayText = displayText;
    }

    public static String getDisplaytext() {
        return DisplayText;
    }

    public static void setEnvironment(String environment) {
        Environment = environment;
    }

    public static String getEnvironment() {
        return Environment;
    }

    public static void setSignerInfo(String signerinfo) {
        SignerInfo = signerinfo;
    }

    public static String getSignerInfo() {
        return SignerInfo;
    }

    public static String getURL() {
        return URL;
    }

    public static String getTmpFolder() {
        return TmpFolder;
    }

    public static void setTmpFolder(String tmpFolder) {
        TmpFolder = tmpFolder;
    }

}
