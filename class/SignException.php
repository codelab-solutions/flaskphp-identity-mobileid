<?php


	/**
	 *
	 *   FlaskPHP-Identity-MobileID
	 *   --------------------------
	 *   Signing exception
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\MobileID;
	use Codelab\FlaskPHP;


	class SignException extends FlaskPHP\Exception\Exception
	{
	}


?>