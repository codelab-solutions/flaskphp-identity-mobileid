<?php


	/**
	 *
	 *   FlaskPHP-Identity-MobileID
	 *   --------------------------
	 *   Authentication provider for Mobile ID
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\MobileID;
	use Codelab\FlaskPHP;


	class Authenticate
	{


		/**
		 *   Dev mode?
		 *   @var bool
		 *   @access public
		 */

		public $devMode = false;


		/**
		 *   Service name
		 *   @var string
		 *   @access public
		 */

		public $serviceName = null;


		/**
		 *   Service message
		 *   @var string
		 *   @access public
		 */

		public $serviceMessage = '';


		/**
		 *   Service language
		 *   @var string
		 *   @access public
		 */

		public $serviceLanguage = 'EST';


		/**
		 *   Service language mapping
		 *   @var array
		 *   @static
		 *   @access public
		 */

		public static $serviceLanguageMap = [
			'et' => 'EST',
			'en' => 'ENG',
			'ru' => 'RUS',
			'lv' => 'LAT',
			'lt' => 'LIT',
			'fi' => 'FIN'
		];


		/**
		 *   SP challenge
		 *   @var string
		 *   @access private
		 */

		private $spChallenge = null;



		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $devMode
		 *   @throws AuthenticateException
		 *   @return Authenticate
		 *
		 */

		public function __construct( bool $devMode=null )
		{
			// Add locale
			Flask()->Locale->addLocalePath(__DIR__.'/../locale');

			// Init
			$this->initMobileID($devMode);
		}


		/**
		 *
		 *   Init the provider
		 *   -----------------
		 *   @access public
		 *   @param bool $devMode Force dev environment
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function initMobileID( bool $devMode=null )
		{
			// Dev mode
			if ($devMode!==null)
			{
				$this->devMode=$devMode;
			}
			else
			{
				$this->devMode=Flask()->Debug->devEnvironment;
			}

			// Service name
			if (Flask()->Config->get('identity.mobileid.servicename'))
			{
				$this->serviceName=Flask()->Config->get('identity.mobileid.servicename');
			}
			elseif (!$this->devMode)
			{
				throw new FlaskPHP\Exception\InvalidParameterException('Missing identity.mobileid.service configuration directive');
			}

			// Language
			if (Flask()->Config->get('identity.mobileid.language'))
			{
				$this->serviceLanguage=Flask()->Config->get('identity.mobileid.language');
			}
			else
			{
				$this->serviceLanguage=oneof(static::$serviceLanguageMap[Flask()->Locale->localeLanguage],'EST');
			}

			// Default service message
			if (Flask()->Config->get('identity.mobileid.servicemessage'))
			{
				$this->serviceMessage=Flask()->Config->get('identity.mobileid.servicemessage');
			}
		}


		/**
		 *
		 *   Init SOAP client
		 *   ----------------
		 *   @access private
		 *   @return \SoapClient
		 *
		 */

		private function initSoapClient()
		{
			// SOAP options
			$streamOptions=[
				'http' => [
					'user_agent' => 'PHPSoapClient'
				]
			];
			if (Flask()->Config->get('identity.mobileid.bindto'))
			{
				$bindTo=Flask()->Config->get('identity.mobileid.bindto');
				if (mb_strpos($bindTo,':')===false) $bindTo.=':0';
				$streamOptions['socket']['bindto']=$bindTo;
			}
			$streamContext=stream_context_create($streamOptions);
			$soapOptions = array(
				'cache_wsdl' => WSDL_CACHE_MEMORY,
				'stream_context' => $streamContext,
				'trace' => true,
				'encoding' => 'utf-8',
				'classmap' => [[
					'MobileAuthenticateResponse' => 'MobileAuthenticateResponse'
				]],
			);

			// Init SOAP client
			if ($this->devMode)
			{
				$WSDL='https://tsp.demo.sk.ee/dds.wsdl';
				$this->spChallenge='00000000000000000000';
				$this->serviceName='Testimine';
			}
			else
			{
				$WSDL='https://digidocservice.sk.ee/?wsdl';
				$this->spChallenge='00000010000002000040';
			}

			$soapClient=new \SoapClient($WSDL, $soapOptions);
			return $soapClient;
		}


		/**
		 *
		 *   Start Mobile ID auth
		 *   --------------------
		 *   @access public
		 *   @param string $phoneNo Phone number (372xxxxxxx)
		 *   @param string $serviceMessage Service message
		 *   @throws AuthenticateException
		 *   @return AuthenticateResponse
		 *
		 */

		public function startAuth( string $phoneNo, string $serviceMessage=null )
		{
			try
			{
				// Remove leading country code
				if (!preg_match("/^\+\d+$/",$phoneNo)) throw new SignException('Invalid phoneNo format, must be +xxxxxxxx.');
				$phoneNo=preg_replace('/^\+/','',$phoneNo);

				// Set service message
				if ($serviceMessage!==null)
				{
					$this->serviceMessage=$serviceMessage;
				}

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->MobileAuthenticate(
					'',
					'',
					$phoneNo,
					$this->serviceLanguage,
					$this->serviceName,
					$this->serviceMessage,
					$this->spChallenge,
					'asynchClientServer',
					null,
					false,
					false
				);

				// Success
				if (!empty($soapResponse['UserIDCode']) && !empty($soapResponse['Sesscode']) && !empty($soapResponse['ChallengeID']))
				{
					// Save Mobiil-ID data to session
					Flask()->Session->set('identity.mobileid.authenticate.response',serialize($soapResponse));
					Flask()->Session->set('identity.mobileid.authenticate.sesscode',strval($soapResponse['Sesscode']));
					Flask()->Session->set('identity.mobileid.authenticate.phoneno',$phoneNo);

					// Return response
					$response=new AuthenticateResponse();
					$response->status='pending';
					$response->challengeResponse=$soapResponse['ChallengeID'];
					return $response;
				}

				// Fail
				else
				{
					throw new AuthenticateException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (!empty($soapFault->detail->message))
				{
					throw new AuthenticateException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new AuthenticateException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Check Mobile ID auth status
		 *   ---------------------------
		 *   @access public
		 *   @throws AuthenticateException
		 *   @return AuthenticateResponse
		 *
		 */

		public function checkAuthStatus()
		{
			global $LAB;
			try
			{
				// Check
				$midResponse=unserialize(Flask()->Session->get('identity.mobileid.authenticate.response'));
				$sessCode=Flask()->Session->get('identity.mobileid.authenticate.sesscode');
				if (empty($midResponse['UserIDCode'])) throw new AuthenticateException('Error reading session data.');
				if (empty($sessCode)) throw new AuthenticateException('Error reading session data.');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->GetMobileAuthenticateStatus(
					$sessCode,
					false
				);

				// Success
				$success=false;
				if (!empty($soapResponse['Status']))
				{
					switch (strval($soapResponse['Status']))
					{
						// In progress
						case 'OUTSTANDING_TRANSACTION':
							$response=new AuthenticateResponse();
							$response->status='pending';
							break;

						// Success
						case 'USER_AUTHENTICATED':
							$response=new AuthenticateResponse();
							$response->status='success';
							$response->firstName=$midResponse['UserGivenname'];
							$response->lastName=$midResponse['UserSurname'];
							$response->idCode=$midResponse['UserIDCode'];
							$response->phoneNo='+'.Flask()->Session->get('identity.mobileid.authenticate.phoneno');
							Flask()->Session->set('identity.mobileid.authenticate.response','');
							Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
							Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
							break;

						// Error
						default:
							Flask()->Session->set('identity.mobileid.authenticate.response','');
							Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
							Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
							$response=new AuthenticateResponse();
							$response->status='error';
							switch (strval($soapResponse['Status']))
							{
								case 'MID_NOT_READY':
									$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.NotReady ]]');
									break;
								case 'SENDING_ERROR':
									$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.RequestFailed ]]');
									break;
								case 'USER_CANCEL':
									$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.UserCancelledAuth ]]');
									break;
								case 'INTERNAL_ERROR':
									$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.TechnicalError ]]');
									break;
								case 'SIM_ERROR':
									$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.SIMError ]]');
									break;
								case 'PHONE_ABSENT':
									$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.PhoneAbsent ]]');
									break;
								default:
									$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.UnknownError ]]');
									break;
							}
							break;
					}
					return $response;
				}
				else
				{
					throw new AuthenticateException('Error talking to the Mobile ID service');
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (!empty($soapFault->detail->message))
				{
					throw new AuthenticateException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new AuthenticateException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


	}


?>