<?php


	/**
	 *
	 *   FlaskPHP-Identity-MobileID
	 *   --------------------------
	 *   Authentication exception
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\MobileID;
	use Codelab\FlaskPHP;


	class AuthenticateException extends FlaskPHP\Exception\Exception
	{
	}


?>