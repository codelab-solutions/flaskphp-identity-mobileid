<?php


	/**
	 *
	 *   FlaskPHP-Identity-MobileID
	 *   --------------------------
	 *   Digital signing provider for Mobile ID
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\MobileID;
	use Codelab\FlaskPHP;


	class Sign
	{


		/**
		 *   Dev mode?
		 *   @var bool
		 *   @access public
		 */

		public $devMode = false;


		/**
		 *   Service name
		 *   @var string
		 *   @access public
		 */

		public $serviceName = null;


		/**
		 *   Service message
		 *   @var string
		 *   @access public
		 */

		public $serviceMessage = '';


		/**
		 *   Service language
		 *   @var string
		 *   @access public
		 */

		public $serviceLanguage = 'EST';


		/**
		 *   Service language mapping
		 *   @var array
		 *   @static
		 *   @access public
		 */

		public static $serviceLanguageMap = [
			'et' => 'EST',
			'en' => 'ENG',
			'ru' => 'RUS',
			'lv' => 'LAT',
			'lt' => 'LIT',
			'fi' => 'FIN'
		];



		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $devMode
		 *   @throws SignException
		 *   @return Sign
		 *
		 */

		public function __construct( bool $devMode=null )
		{
			// Add locale
			Flask()->Locale->addLocalePath(__DIR__.'/../locale');

			// Init
			$this->initMobileID($devMode);
		}


		/**
		 *
		 *   Init the provider
		 *   -----------------
		 *   @access public
		 *   @param bool $devMode Force dev environment
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function initMobileID( bool $devMode=null )
		{
			// Dev mode
			if ($devMode!==null)
			{
				$this->devMode=$devMode;
			}
			else
			{
				$this->devMode=Flask()->Debug->devEnvironment;
			}

			// Service name
			if (Flask()->Config->get('identity.mobileid.servicename'))
			{
				$this->serviceName=Flask()->Config->get('identity.mobileid.servicename');
			}
			elseif (!$this->devMode)
			{
				throw new FlaskPHP\Exception\InvalidParameterException('Missing identity.mobileid.service configuration directive');
			}

			// Language
			if (Flask()->Config->get('identity.mobileid.language'))
			{
				$this->serviceLanguage=Flask()->Config->get('identity.mobileid.language');
			}
			else
			{
				$this->serviceLanguage=oneof(static::$serviceLanguageMap[Flask()->Locale->localeLanguage],'EST');
			}

			// Default service message
			if (Flask()->Config->get('identity.mobileid.servicemessage'))
			{
				$this->serviceMessage=Flask()->Config->get('identity.mobileid.servicemessage');
			}
		}


		/**
		 *
		 *   Init SOAP client
		 *   ----------------
		 *   @access private
		 *   @return \SoapClient
		 *
		 */

		private function initSoapClient()
		{
			// SOAP options
			$streamOptions=[
				'http' => [
					'user_agent' => 'PHPSoapClient'
				]
			];
			if (Flask()->Config->get('identity.mobileid.bindto'))
			{
				$bindTo=Flask()->Config->get('identity.mobileid.bindto');
				if (mb_strpos($bindTo,':')===false) $bindTo.=':0';
				$streamOptions['socket']['bindto']=$bindTo;
			}
			$streamContext=stream_context_create($streamOptions);
			$soapOptions = array(
				'cache_wsdl' => WSDL_CACHE_MEMORY,
				'stream_context' => $streamContext,
				'trace' => true,
				'encoding' => 'utf-8'
			);

			// Init SOAP client
			if ($this->devMode)
			{
				$WSDL='https://tsp.demo.sk.ee/dds.wsdl';
				$this->serviceName='Testimine';
			}
			else
			{
				$WSDL='https://digidocservice.sk.ee/?wsdl';
			}

			$soapClient=new \SoapClient($WSDL, $soapOptions);
			return $soapClient;
		}


		/**
		 *
		 *   Start signing session
		 *   ---------------------
		 *   @access public
		 *   @param string $digiDocFile Digidoc file (hashcoded)
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function startSession( string $digiDocFile )
		{
			try
			{
				// Check && base64-encode file
				if (!mb_strlen($digiDocFile)) throw new SignException('No file.');
				$digiDocFile=base64_encode($digiDocFile);

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->StartSession(
					'',
					$digiDocFile,
					true
				);
				
				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." StartSession: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK' && intval($soapResponse['Sesscode']))
				{
					// Save Mobiil-ID data to session
					Flask()->Session->set('identity.mobileid.sign.sesscode',intval($soapResponse['Sesscode']));

					// Return response
					$response=new SignResponse();
					$response->status='OK';
					$response->sessCode=intval($soapResponse['Sesscode']);
					$response->signedDocInfo=$soapResponse['SignedDocInfo'];
					return $response;
				}

				// Fail
				else
				{
					if (Flask()->Debug->debugOn)
					{
						file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug.bdoc',base64_decode($digiDocFile));
					}
					throw new SignException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." StartSession SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.bdoc',base64_decode($digiDocFile));
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Start Mobile ID sign process
		 *   ----------------------------
		 *   @access public
		 *   @param string $phoneNo Phone number (372xxxxxxx)
		 *   @param string $signerIdCode Signer's ID code
		 *   @param string $signerCountry Signer's country
		 *   @param array $signerInfo Additional signer info
		 *   @param string $serviceMessage Service message
		 *   @param int $sessionCode Session code
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function startSigning( string $phoneNo='', string $signerIdCode='', string $signerCountry='', array $signerInfo=null, string $serviceMessage=null, int $sessionCode=null )
		{
			try
			{
				// Check session
				$sessCode=oneof(Flask()->Session->get('identity.mobileid.sign.sesscode'), $sessionCode);
				if (!intval($sessCode)) throw new SignException('SK session code not found. Session not initialized?');
                //throw new SignException("test".var_dump_str($signerInfo));

				// Check that either phone number or ID code exists
				if (!mb_strlen($phoneNo) && !mb_strlen($signerIdCode)) throw new SignException('Either phoneNo or signedIdCode is required.');
				if (mb_strlen($phoneNo) && !preg_match("/^\+\d+$/",$phoneNo)) throw new SignException('Invalid phoneNo format, must be +xxxxxxxx.');

				// Remove leading country code
				$phoneNo=preg_replace('/^\+/','',$phoneNo);

				// Set service message
				if ($serviceMessage!==null)
				{
					$this->serviceMessage=$serviceMessage;
				}

				// Additional signer info
				$signerInfoRole=(($signerInfo!==null && array_key_exists('role',$signerInfo))?$signerInfo['role']:'');
				$signerInfoCity=(($signerInfo!==null && array_key_exists('city',$signerInfo))?$signerInfo['city']:'');
				$signerInfoState=(($signerInfo!==null && array_key_exists('state',$signerInfo))?$signerInfo['state']:'');
				$signerInfoPostalCode=(($signerInfo!==null && array_key_exists('postalcode',$signerInfo))?$signerInfo['postalcode']:'');
				$signerInfoCountry=(($signerInfo!==null && array_key_exists('country',$signerInfo))?$signerInfo['country']:'');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->MobileSign(
					$sessCode,
					$signerIdCode,
					$signerCountry,
					$phoneNo,
					$this->serviceName,
					$this->serviceMessage,
					$this->serviceLanguage,
					$signerInfoRole,
					$signerInfoCity,
					$signerInfoState,
					$signerInfoPostalCode,
					$signerInfoCountry,
					'LT_TM',
					'asynchClientServer',
					null,
					true,
					true
				);

				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." MobileSign: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK')
				{
					// Return response
					$response=new SignResponse();
					$response->status='pending';
					$response->challengeResponse=$soapResponse['ChallengeID'];
					return $response;
				}

				// Fail
				else
				{
					if (!empty($soapResponse['Status']))
					{
						throw new SignException('[[ FLASK.COMMON.Error ]]: '.$soapResponse['Status']);
					}
					else
					{
						throw new SignException('Error talking to the Mobile ID service');
					}
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." MobileSign SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Check Mobile ID auth status
		 *   ---------------------------
		 *   @access public
		 *   @param int $sessionCode Session code
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function checkSignStatus( int $sessionCode=null )
		{
			global $LAB;
			try
			{
				// Check session
				$sessCode=oneof(Flask()->Session->get('identity.mobileid.sign.sesscode'), $sessionCode);
				if (!intval($sessCode)) throw new SignException('SK session code not found. Session not initialized?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->GetStatusInfo(
					$sessCode,
					true,
					false
				);

				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." GetStatusInfo: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				$success=false;
				if (!empty($soapResponse['Status']))
				{
					switch (strval($soapResponse['Status']))
					{
						// OK
						case 'OK':
							switch ($soapResponse['StatusCode'])
							{
								// Pending
								case 'OUTSTANDING_TRANSACTION':
									$response=new SignResponse();
									$response->status='pending';
									break;

								// OK, signed
								case 'SIGNATURE':
									$response=new SignResponse();
									$response->status='success';
									$response->signedDocInfo=$soapResponse['SignedDocInfo'];
									break;

								// Default
								default:
									$response=new SignResponse();
									$response->status='error';
									switch ($soapResponse['StatusCode'])
									{
										case 'REVOKED_CERTIFICATE':
											$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.CertificateRevoked ]]');
											break;
										case 'MID_NOT_READY':
											$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.NotReady ]]');
											break;
										case 'SENDING_ERROR':
											$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.RequestFailed ]]');
											break;
										case 'USER_CANCEL':
											$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.UserCancelledSign ]]');
											break;
										case 'INTERNAL_ERROR':
											$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.TechnicalError ]]');
											break;
										case 'SIM_ERROR':
											$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.SIMError ]]');
											break;
										case 'PHONE_ABSENT':
											$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.PhoneAbsent ]]');
											break;
										default:
											$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.UnknownError ]]');
											break;
									}
									break;
							}
							break;

						// Error
						default:
							$response=new SignResponse();
							$response->status='error';
							$response->error=FlaskPHP\Template\Template::parseContent('[[ FLASK.IDENTITY.MobileID.Error.UnknownError ]]');
							break;
					}
					return $response;
				}
				else
				{
					throw new SignException('Error talking to the Mobile ID service');
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." GetStatusInfo SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Get signed document
		 *   -------------------
		 *   @access public
		 *   @param int $sessionCode Session code
		 *   @throws SignException
		 *   @return string
		 *
		 */

		public function getSignedDoc( int $sessionCode=null )
		{
			try
			{
				// Check session
				$sessCode=oneof(Flask()->Session->get('identity.mobileid.sign.sesscode'), $sessionCode);
				if (!intval($sessCode)) throw new SignException('SK session code not found. Session not initialized or already closed?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->GetSignedDoc(
					$sessCode
				);

				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." GetSignedDoc: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK')
				{
					return base64_decode($soapResponse['SignedDocData']);
				}

				// Fail
				else
				{
					throw new SignException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." GetSignedDoc SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Close signing session
		 *   @access public
		 * 	 @param int $sessionCode Session code
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function closeSession( int $sessionCode=null )
		{
			try
			{
				// Check session
				$sessCode=oneof(Flask()->Session->get('identity.mobileid.sign.sesscode'), $sessionCode);
				if (!intval($sessCode)) throw new SignException('SK session code not found. Session not initialized or already closed?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->CloseSession(
					$sessCode
				);

				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." CloseSession: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse=='OK')
				{
					// Save Mobiil-ID data to session
					Flask()->Session->set('identity.mobileid.sign.sesscode',null);

					// Return response
					$response=new SignResponse();
					$response->status='OK';
					return $response;
				}

				// Fail
				else
				{
					throw new SignException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',date('Y-m-d H:i:s')." CloseSession SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the Mobile ID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


	}


?>