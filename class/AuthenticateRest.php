<?php


/**
 *
 *   FlaskPHP-Identity-MobileID
 *   --------------------------
 *   Authentication provider for Mobile ID
 *
 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
 *   @license  https://www.flaskphp.com/LICENSE MIT
 *
 */


namespace Codelab\FlaskPHP\Identity\MobileID;
use Codelab\FlaskPHP;
use Sk\Mid\DisplayTextFormat;
use Sk\Mid\Language\ENG;
use Sk\Mid\Language\EST;
use Sk\Mid\Language\Language;
use Sk\Mid\Language\RUS;
use Sk\Mid\Language\LIT;
use Sk\Mid\MobileIdAuthenticationHashToSign;
use Sk\Mid\MobileIdClient;
use Sk\Mid\Rest\Dao\Request\AuthenticationRequest;
use Sk\Mid\Util\MidInputUtil;


class AuthenticateRest
{


	/**
	 *   Dev mode?
	 *   @var bool
	 *   @access public
	 */

	public $devMode = false;


	/**
	 *   Service name
	 *   @var string
	 *   @access public
	 */

	public $serviceName = null;


	/**
	 *   Service message
	 *   @var string
	 *   @access public
	 */

	public $serviceMessage = '';


	/**
	 *   Service language
	 *   @var string
	 *   @access public
	 */

	public $serviceLanguage = 'EST';


	/**
	 *   Service language mapping
	 *   @var array
	 *   @static
	 *   @access public
	 */

	public static $serviceLanguageMap = [
		'et' => 'EST',
		'en' => 'ENG',
		'ru' => 'RUS',
		'lt' => 'LIT'
	];


	/**
	 *   SP challenge
	 *   @var string
	 *   @access private
	 */

	private $spChallenge = null;


	/**
	 *   Replying party UUID
	 *   @var string
	 *   @access private
	 */

	private $relyingPartyUUID = null;


	/**
	 *   Network interface
	 *   @var network interface
	 *   @access private
	 */

	private $networkInterface = null;



	/**
	 *
	 *   Constructor
	 *   -----------
	 *   @access public
	 *   @param bool $devMode
	 *   @throws AuthenticateException
	 *   @return Authenticate
	 *
	 */

	public function __construct( bool $devMode=null )
	{
		// Add locale
		Flask()->Locale->addLocalePath(__DIR__.'/../locale');

		// Init
		$this->initMobileID($devMode);
	}


	/**
	 *
	 *   Init the provider
	 *   -----------------
	 *   @access public
	 *   @param bool $devMode Force dev environment
	 *   @throws \Exception
	 *   @return void
	 *
	 */

	public function initMobileID( bool $devMode=null )
	{
		// Dev mode
		if ($devMode!==null)
		{
			$this->devMode=$devMode;
		}
		else
		{
			$this->devMode=Flask()->Debug->devEnvironment;
		}

		// Service name
		if (Flask()->Config->get('identity.mobileid.servicename'))
		{
			$this->serviceName=Flask()->Config->get('identity.mobileid.servicename');
		}
		elseif (!$this->devMode)
		{
			throw new FlaskPHP\Exception\InvalidParameterException('Missing identity.mobileid.service configuration directive');
		}

		// Language
		if (Flask()->Config->get('identity.mobileid.language'))
		{
			$this->serviceLanguage=Flask()->Config->get('identity.mobileid.language');
		}
		else
		{
			$this->serviceLanguage=oneof(static::$serviceLanguageMap[Flask()->Locale->localeLanguage],'EST');
		}

		// Default service message
		if (Flask()->Config->get('identity.mobileid.servicemessage'))
		{
			$this->serviceMessage=Flask()->Config->get('identity.mobileid.servicemessage');
		}

		// Get Live UUID
		if (Flask()->Config->get('identity.mobileid.uuid'))
		{
			$this->relyingPartyUUID=Flask()->Config->get('identity.mobileid.uuid');
		}

		// Network interface
		if (Flask()->Config->get('identity.mobileid.bindto'))
		{
			$this->networkInterface=Flask()->Config->get('identity.mobileid.bindto');
		}
	}


	/**
	 *
	 *   Init client
	 *   ----------------
	 *   @access private
	 *   @return \Client
	 *
	 */

	private function initClient()
	{

		// Init client
		if ($this->devMode)
		{
			$Client=MobileIdClient::newBuilder()
				->withRelyingPartyUUID('00000000-0000-0000-0000-000000000000')
				->withRelyingPartyName('Testimine')
				->withHostUrl('https://tsp.demo.sk.ee/mid-api')
				->withLongPollingTimeoutSeconds(60)
				->withPollingSleepTimeoutSeconds(2)
				->withSslPinnedPublicKeys("sha256//Rhm2BxU8LheLZP664D3J4yIZCjkU1EQDRJnrMRggwTU=");
		}
		else
		{
			$Client = MobileIdClient::newBuilder()
				->withRelyingPartyUUID($this->relyingPartyUUID)
				->withRelyingPartyName($this->serviceName)
				->withHostUrl('https://mid.sk.ee/mid-api')
				->withLongPollingTimeoutSeconds(60)
				->withPollingSleepTimeoutSeconds(2)
				->withSslPinnedPublicKeys("sha256//+aKQHcCEBu5zSlcNENV/u/Hug0s5E4j6lezohRhFBm4=;sha256//dFJCeFlwS05emnSu2Ka7F/4Zxm6pxryNKHais7t/+2o=;sha256//DEdS2LkzDSKK66waYbjBi2rViBThSbrnD1Jaj87AMOE=;sha256//DEdS2LkzDSKK66waYbjBi2rViBThSbrnD1Jaj87AMOE=;sha256//+1w894tu6TxJSfmUfCL4wQqQ3hqfsLLJvi3UyY17i/Q=");
		}

		// Bind network interface
		if (!empty($this->networkInterface))
		{
			$Client->withNetworkInterface($this->networkInterface);
		}

		// Return
		return $Client->build();
	}


	/**
	 *
	 *   Start Mobile ID auth
	 *   --------------------
	 *   @access public
	 *   @param string $phoneNo Phone number (372xxxxxxx)
	 *   @param string $idcode national ID code
	 *   @param string $serviceMessage Service message
	 *   @throws AuthenticateException
	 *   @return AuthenticateResponse
	 *
	 */

	public function startAuth( string $phoneNo, string $idcode, string $serviceMessage=null )
	{
		try
		{
			// Remove leading country code
			if (!preg_match("/^\+\d+$/",$phoneNo)) throw new SignException('Invalid phoneNo format, must be +xxxxxxxx.');

			try {
				$phoneNumber = MidInputUtil::getValidatedPhoneNumber($phoneNo);
				$nationalIdentityNumber = MidInputUtil::getValidatedNationalIdentityNumber($idcode);
			}
			catch (InvalidPhoneNumberException $e) {
				throw new SignException('The phone number you entered is invalid');
			}
			catch (InvalidNationalIdentityNumberException $e) {
				throw new SignException('The national identity number you entered is invalid');
			}

			$phoneNo=preg_replace('/^\+/','',$phoneNo);

			// Set service message
			if ($serviceMessage!==null)
			{
				$this->serviceMessage=$serviceMessage;
			}

			// Init client
			$Client=$this->initClient();


			$authenticationHash = MobileIdAuthenticationHashToSign::generateRandomHashOfDefaultType();
			$verificationCode = $authenticationHash->calculateVerificationCode();

			// Lang
			switch ($this->serviceLanguage)
			{
				case ('EST'):
					$lang=EST::asType();
					break;
				case ('RUS'):
					$lang=RUS::asType();
					break;
				case ('ENG'):
					$lang=ENG::asType();
					break;
				case ('LIT'):
					$lang=LIT::asType();
					break;
			}

			$request = AuthenticationRequest::newBuilder()
				->withPhoneNumber($phoneNumber)
				->withNationalIdentityNumber($nationalIdentityNumber)
				->withHashToSign($authenticationHash)
				->withLanguage($lang)
				->withDisplayText($this->serviceMessage)
				->withDisplayTextFormat(DisplayTextFormat::GSM7)
				->build();

			$Response = $Client->getMobileIdConnector()->initAuthentication($request);


			// Save Mobiil-ID data to session
			Flask()->Session->set('identity.mobileid.authenticate.authhash',$authenticationHash);
			Flask()->Session->set('identity.mobileid.authenticate.sesscode',$Response->getSessionID());
			Flask()->Session->set('identity.mobileid.authenticate.phoneno',$phoneNo);

			// Return response
			$response=new AuthenticateResponse();
			$response->status='pending';
			$response->challengeResponse=$verificationCode;
			return $response;
		}
		catch (NotMidClientException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException("You are not a Mobile-ID client or your Mobile-ID certificates are revoked. Please contact your mobile operator.");
		}
		catch (UnauthorizedException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException('Integration error with Mobile-ID. Invalid MID credentials');
		}
		catch (MissingOrInvalidParameterException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException('Problem with MID integration');
		}
		catch (MidInternalErrorException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException('MID internal error');
		}
	}


	/**
	 *
	 *   Check Mobile ID auth status
	 *   ---------------------------
	 *   @access public
	 *   @throws AuthenticateException
	 *   @return AuthenticateResponse
	 *
	 */

	public function checkAuthStatus()
	{
		global $LAB;
		try
		{
			// Check
			$authHash=Flask()->Session->get('identity.mobileid.authenticate.authhash');
			$sessCode=Flask()->Session->get('identity.mobileid.authenticate.sesscode');
			if (empty($authHash)) throw new AuthenticateException('Error reading session data.');
			if (empty($sessCode)) throw new AuthenticateException('Error reading session data.');

			// Init SOAP client
			$Client=$this->initClient();

			// Make request
			$finalSessionStatus = $Client
				->getSessionStatusPoller()
				->fetchFinalSessionStatus($sessCode);

			$authenticatedPerson = $Client
				->createMobileIdAuthentication($finalSessionStatus, $authHash)
				->constructAuthenticationIdentity();

			// Success
			if (!empty($authenticatedPerson))
			{
				// Success
				$response=new AuthenticateResponse();
				$response->status='success';
				$response->firstName=$authenticatedPerson->getGivenName();
				$response->lastName=$authenticatedPerson->getSurName();
				$response->idCode=$authenticatedPerson->getIdentityCode();
				$response->phoneNo='+'.Flask()->Session->get('identity.mobileid.authenticate.phoneno');
				Flask()->Session->set('identity.mobileid.authenticate.authhash','');
				Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
				Flask()->Session->set('identity.mobileid.authenticate.phoneno','');

				return $response;
			}
			else
			{
				$response=new AuthenticateResponse();
				$response->status='pending';
				return $response;
			}
		}
		catch (UserCancellationException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException("You cancelled operation from your phone.");
		}
		catch (MidSessionTimeoutException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException("You didn't type in PIN code into your phone or there was a communication error.");
		}
		catch (PhoneNotAvailableException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException("Unable to reach your phone. Please make sure your phone has mobile coverage.");
		}
		catch (DeliveryException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException("Communication error. Unable to reach your phone.");
		}
		catch (InvalidUserConfigurationException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException("Mobile-ID configuration on your SIM card differs from what is configured on service provider's side. Please contact your mobile operator.");
		}
		catch (MidSessionNotFoundException | MissingOrInvalidParameterException | UnauthorizedException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException("Client side error with mobile-ID integration. Error code:". $e->getCode());
		}
		catch (NotMidClientException $e) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			// if user is not MID client then this exception is thrown and caught already during first request (see above)
			throw new AuthenticateException("You are not a Mobile-ID client or your Mobile-ID certificates are revoked. Please contact your mobile operator.");
		}
		catch (MidInternalErrorException $internalError) {
			Flask()->Session->set('identity.mobileid.authenticate.authhash','');
			Flask()->Session->set('identity.mobileid.authenticate.sesscode','');
			Flask()->Session->set('identity.mobileid.authenticate.phoneno','');
			throw new AuthenticateException("Something went wrong with Mobile-ID service");
		}
	}


}


?>
