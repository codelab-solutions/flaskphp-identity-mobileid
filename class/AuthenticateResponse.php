<?php


	/**
	 *
	 *   FlaskPHP-Identity-MobileID
	 *   --------------------------
	 *   Mobile ID authentication response
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\MobileID;


	class AuthenticateResponse
	{


		/**
		 *   Response status
		 *   @var string
		 *     pending  -  request pending
		 *     success  -  successfully authenticated
		 *     error    -  error
		 */

		public $status = null;


		/**
		 *   Error message
		 *   @var string
		 *   @access public
		 */

		public $error = null;


		/**
		 *   Challenge response (verification code)
		 *   @var string
		 *   @access public
		 */

		public $challengeResponse = null;


		/**
		 *   First name
		 *   @var string
		 *   @access public
		 */

		public $firstName = null;


		/**
		 *   Last name
		 *   @var string
		 *   @access public
		 */

		public $lastName = null;


		/**
		 *   ID code
		 *   @var string
		 *   @access public
		 */

		public $idCode = null;


		/**
		 *   Mobile phone number
		 *   @var string
		 *   @access public
		 */

		public $phoneNo = null;


		/**
		 *
		 *   Get status
		 *   ----------
		 *   @access public
		 *   return string
		 *
		 */

		public function getStatus()
		{
			return $this->status;
		}


		/**
		 *
		 *   Is pending?
		 *   -----------
		 *   @access public
		 *   return bool
		 *
		 */

		public function isPending()
		{
			return ($this->status==='pending'?true:false);
		}


		/**
		 *
		 *   Was a success?
		 *   --------------
		 *   @access public
		 *   return bool
		 *
		 */

		public function isSuccess()
		{
			return ($this->status==='success'?true:false);
		}


		/**
		 *
		 *   Was an error?
		 *   -------------
		 *   @access public
		 *   return bool
		 *
		 */

		public function isError()
		{
			return ($this->status==='error'?true:false);
		}


		/**
		 *
		 *   Get challenge response
		 *   ----------------------
		 *   @access public
		 *   return string
		 *
		 */

		public function getChallengeResponse()
		{
			return $this->challengeResponse;
		}


		/**
		 *
		 *   Get first name
		 *   --------------
		 *   @access public
		 *   return string
		 *
		 */

		public function getFirstName()
		{
			return $this->firstName;
		}


		/**
		 *
		 *   Get last name
		 *   -------------
		 *   @access public
		 *   return string
		 *
		 */

		public function getLastName()
		{
			return $this->lastName;
		}


		/**
		 *
		 *   Get ID code
		 *   -----------
		 *   @access public
		 *   return string
		 *
		 */

		public function getIDCode()
		{
			return $this->idCode;
		}


	}


?>