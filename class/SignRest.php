<?php


	/**
	 *
	 *   FlaskPHP-Identity-MobileID
	 *   --------------------------
	 *   Digital signing provider for Mobile ID
	 *
	 * @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 * @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\MobileID;

	use Codelab\FlaskPHP;
	use Sk\Mid\Exception\DeliveryException;
	use Sk\Mid\Exception\InvalidNationalIdentityNumberException;
	use Sk\Mid\Exception\InvalidPhoneNumberException;
	use Sk\Mid\Exception\InvalidUserConfigurationException;
	use Sk\Mid\Exception\MidDeliveryException;
	use Sk\Mid\Exception\MidInternalErrorException;
	use Sk\Mid\Exception\MidInvalidNationalIdentityNumberException;
	use Sk\Mid\Exception\MidInvalidPhoneNumberException;
	use Sk\Mid\Exception\MidInvalidUserConfigurationException;
	use Sk\Mid\Exception\MidNotMidClientException;
	use Sk\Mid\Exception\MidPhoneNotAvailableException;
	use Sk\Mid\Exception\MidSessionNotFoundException;
	use Sk\Mid\Exception\MidSessionTimeoutException;
	use Sk\Mid\Exception\MidUnauthorizedException;
	use Sk\Mid\Exception\MidUserCancellationException;
	use Sk\Mid\Exception\MissingOrInvalidParameterException;
	use Sk\Mid\Exception\MobileIdException;
	use Sk\Mid\Exception\NotMidClientException;
	use Sk\Mid\Exception\PhoneNotAvailableException;
	use Sk\Mid\Exception\UnauthorizedException;
	use Sk\Mid\Exception\UserCancellationException;
	use Sk\Mid\VerificationCodeCalculator;


	class SignRest
	{


		/**
		 *   Dev mode?
		 *   @var bool
		 *   @access public
		 */

		public $devMode = false;

		/**
		 *   Java class path
		 *   @var string
		 *   @access private
		 */

		private $javaClassPath = ':./libs/logging/*:./libs/digidoc4j/*:./libs/javax.ws.rs/*:./libs/json/*:./libs/mid-rest-java-client/*:./libs/org.junit/*:./libs/org.springframework/*';

		/**
		 *   Service name
		 *   @var string
		 *   @access public
		 */

		public $serviceName = null;


		/**
		 *   Service message
		 *   @var string
		 *   @access public
		 */

		public $serviceMessage = '';


		/**
		 *   Service language
		 *   @var string
		 *   @access public
		 */

		public $serviceLanguage = 'EST';


		/**
		 *   Service language mapping
		 *   @var array
		 *   @static
		 *   @access public
		 */

		public static $serviceLanguageMap = [
			'et' => 'EST',
			'en' => 'ENG',
			'ru' => 'RUS',
			'lt' => 'LIT'
		];


		/**
		 *   Replying party UUID
		 *   @var string
		 *   @access private
		 */

		private $relyingPartyUUID = null;


		/**
		 *   URL
		 *   @var string
		 *   @access private
		 */

		private $url = null;


		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $devMode
		 *   @throws SignException
		 *   @return Sign
		 *
		 */

		public function __construct( bool $devMode = null )
		{
			// Add locale
			Flask()->Locale->addLocalePath(__DIR__ . '/../locale');

			// Init
			$this->initMobileID($devMode);
		}


		/**
		 *
		 *   Init the provider
		 *   -----------------
		 *   @access public
		 *   @param bool $devMode Force dev environment
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function initMobileID( bool $devMode = null )
		{
			// Dev mode
			if ($devMode !== null)
			{
				$this->devMode = $devMode;
			}
			else
			{
				$this->devMode = Flask()->Debug->devEnvironment;
			}

			// Service name
			if (Flask()->Config->get('identity.mobileid.servicename'))
			{
				$this->serviceName = Flask()->Config->get('identity.mobileid.servicename');
			}
			elseif (!$this->devMode)
			{
				throw new FlaskPHP\Exception\InvalidParameterException('Missing identity.mobileid.service configuration directive');
			}

			// Language
			if (Flask()->Config->get('identity.mobileid.language'))
			{
				$this->serviceLanguage = Flask()->Config->get('identity.mobileid.language');
			}
			else
			{
				$this->serviceLanguage = oneof(static::$serviceLanguageMap[Flask()->Locale->localeLanguage], 'EST');
			}

			// Default service message
			if (Flask()->Config->get('identity.mobileid.servicemessage'))
			{
				$this->serviceMessage = Flask()->Config->get('identity.mobileid.servicemessage');
			}

			// Get Live UUID
			if (Flask()->Config->get('identity.mobileid.uuid'))
			{
				$this->relyingPartyUUID = Flask()->Config->get('identity.mobileid.uuid');
			}
		}


		/**
		 *
		 *   Init Mobile ID Client
		 *   ----------------
		 *   @access public
		 *   @return SignResponse
		 *
		 */

		public function initMobileIDClient( $idcode, $phoneNo, $displayText, $container, array $signerInfo = null )
		{

			if ($this->devMode)
			{
				$url = 'https://tsp.demo.sk.ee/mid-api';
				$UUID = '00000000-0000-0000-0000-000000000000';
				$name = 'DEMO';
				$environment = 'TEST';
			}
			else
			{
				$url = 'https://mid.sk.ee/mid-api';
				$UUID = $this->relyingPartyUUID;
				$name = $this->serviceName;
				$environment = 'LIVE';
			}

			$uniqueName = uniqid();

			$filename = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.txt';

			$BDOCFile = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.asice';
			file_put_contents($BDOCFile, $container);

			file_put_contents($filename, '');

			Flask()->Session->set('mobileid.temp.output.file', $filename);
			Flask()->Session->set('mobileid.temp.output.name', $uniqueName);

			// Additional signer info
			$SignerInfo = array();
			$SignerInfo['role'] = (($signerInfo !== null && array_key_exists('role', $signerInfo)) ? $signerInfo['role'] : '');
			$SignerInfo['city'] = (($signerInfo !== null && array_key_exists('city', $signerInfo)) ? $signerInfo['city'] : '');
			$SignerInfo['state'] = (($signerInfo !== null && array_key_exists('state', $signerInfo)) ? $signerInfo['state'] : '');
			$SignerInfo['postalcode'] = (($signerInfo !== null && array_key_exists('postalcode', $signerInfo)) ? $signerInfo['postalcode'] : '');
			$SignerInfo['country'] = (($signerInfo !== null && array_key_exists('country', $signerInfo)) ? $signerInfo['country'] : '');

			$SignerInfo = json_encode($SignerInfo);
			$SignerInfo = base64_encode($SignerInfo);

			$displayText = base64_encode($displayText);
			$IDCode = $idcode;
			$cmd = 'java -classpath ' . $this->javaClassPath . ' Sign ' . $url . ' ' . $UUID . ' "' . $name . '" ' . $IDCode . ' ' . $phoneNo . ' ' . $displayText . ' ' . $filename . ' ' . $environment . ' ' . $SignerInfo . ' ' . Flask()->Config->getTmpPath() . ' >/dev/null 2>&1 &';
			$errors = '';

			exec_with_cwd($cmd, realpath(__DIR__ . '/../java/'), $errors);

			// Return response
			$response = new SignResponse();
			$response->status = 'OK';
			$response->filename = $filename;
			$response->bdocfilename = $uniqueName;
			return $response;
		}


		/**
		 *
		 *   Start signing session
		 *   ---------------------
		 *   @access public
		 *   @param string $digiDocFile Digidoc file
		 *   @param string signer $idcode
		 *   @param string signer $phoneNo
		 *   @param string $displayText
		 *   @param array $signerInfo
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function startSession( string $digiDocFile, $idcode, $phoneNo, $displayText, array $signerInfo = array() )
		{
			// Init client
			$clientResponse = $this->initClient($idcode, $phoneNo, $displayText, $digiDocFile, $signerInfo);

			return $clientResponse;
		}


		/**
		 *
		 *   Start signing session
		 *   ---------------------
		 *   @access public
		 *   @param string $filename filename to read from
		 *   @param string $bdocname Digidoc file name
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function getVerificationCode( string $filename, string $bdocname = null )
		{
			try
			{

				$verificationCode = '';

				if ($file = fopen($filename, "r"))
				{
					while (!feof($file))
					{
						$line = fgets($file);

						if (strpos($line, '[verification_code]') !== false)
						{

							$verificationCode = preg_replace("/[^0-9]/", '', $line);
							break;
						}
						elseif (strpos($line, '[error]') !== false || strpos($line, 'Exception in thread "main"') !== false)
						{
							$line = substr($line, 0, strpos($line, ":"));
							$exception = trim(substr($line, strrpos($line, '.') + 1));
							error_log('error ' . $line);
							// Delete temp file
							unlink($filename);
							unlink(Flask()->Config->getTmpPath() . '/' . oneof(Flask()->Session->get('mobileid.temp.output.name'), $bdocname) . '.asice');

							Flask()->Session->set('mobileid.temp.output.file', '');
							Flask()->Session->set('mobileid.temp.output.name', '');
							switch ($exception)
							{
								case 'MidDeliveryException':
									throw new MidDeliveryException($line);
								case 'MidException':
									throw new SignException($line);
								case 'MidInternalErrorException':
									throw new MidInternalErrorException($line);
								case 'MidInvalidNationalIdentityNumberException':
									throw new MidInvalidNationalIdentityNumberException($line);
								case 'MidInvalidPhoneNumberException':
									throw new MidInvalidPhoneNumberException($line);
								case 'MidInvalidUserConfigurationException':
									throw new MidInvalidUserConfigurationException($line);
								case 'MidMissingOrInvalidParameterException':
									throw new MissingOrInvalidParameterException($line);
								case 'MidNotMidClientException':
									throw new MidNotMidClientException($line);
								case 'MidPhoneNotAvailableException':
									throw new MidPhoneNotAvailableException($line);
								case 'MidSessionNotFoundException':
									throw new MidSessionNotFoundException($line);
								case 'MidSessionTimeoutException':
									throw new MidSessionTimeoutException($line);
								case 'MidUnauthorizedException':
									throw new MidUnauthorizedException($line);
								case 'MidUserCancellationException':
									throw new MidUserCancellationException($line);
								default:
									throw new SignException($line);
							}

							break;
						}
					}
					fclose($file);
				}

				if (!empty($verificationCode))
				{
					// Return response
					$response = new SignResponse();
					$response->status = 'pending';
					$response->verificationCode = $verificationCode;
					return $response;
				}
				else
				{
					// Return response
					$response = new SignResponse();
					$response->status = 'OK';
					return $response;
				}
			}
			catch (MidDeliveryException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.DeliveryException]]');
			}
			catch (MidInternalErrorException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidInternalErrorException]]');
			}
			catch (MidInvalidNationalIdentityNumberException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidNationalIdentityNumberException]]');
			}
			catch (MidInvalidPhoneNumberException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidPhoneNumberException]]');
			}
			catch (MidInvalidUserConfigurationException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidUserConfigurationException]]');
			}
			catch (MissingOrInvalidParameterException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MissingOrInvalidParameterException]]');
			}
			catch (MidNotMidClientException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.NotMidClientException]]');
			}
			catch (MidPhoneNotAvailableException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.PhoneNotAvailableException]]');
			}
			catch (MidSessionNotFoundException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidSessionNotFoundException]]');
			}
			catch (MidSessionTimeoutException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidSessionTimeoutException]]');
			}
			catch (MidUnauthorizedException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.UnauthorizedException]]');
			}
			catch (MidUserCancellationException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.UserCancellationException]]');
			}
			catch (\Exception $e)
			{
				switch ($e->getMessage())
				{
					case 'MidDeliveryException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.DeliveryException]]');
					case 'MidException':
					case 'MidInternalErrorException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidInternalErrorException]]');
					case 'MidInvalidNationalIdentityNumberException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidNationalIdentityNumberException]]');
					case 'MidInvalidPhoneNumberException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidPhoneNumberException]]');
					case 'MidInvalidUserConfigurationException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidUserConfigurationException]]');
					case 'MidMissingOrInvalidParameterException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MissingOrInvalidParameterException]]');
					case 'MidNotMidClientException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.NotMidClientException]]');
					case 'MidPhoneNotAvailableException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.PhoneNotAvailableException]]');
					case 'MidSessionNotFoundException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidSessionNotFoundException]]');
					case 'MidSessionTimeoutException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidSessionTimeoutException]]');
					case 'MidUnauthorizedException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.UnauthorizedException]]');
					case 'MidUserCancellationException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.UserCancellationException]]');
					default:
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MobileId.Other]]' . $e->getMessage());
				}
			}
		}


		/**
		 *
		 *   Check Mobile ID auth status
		 *   ---------------------------
		 *   @access public
		 *   @param string $filename filename to read from
		 *   @param string $bdocname Digidoc file name
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function checkStatus( string $filename, string $bdocname = null )
		{
			try
			{
				$exception = '';

				if ($file = fopen($filename, "r"))
				{
					while (!feof($file))
					{
						$line = fgets($file);

						if (strpos($line, '[success]') !== false)
						{
							// Delete temp file
							unlink($filename);

							$Container = FlaskPHP\DigiDoc\DigiDocContainer::openContainer(Flask()->Config->getTmpPath() . '/' . oneof(Flask()->Session->get('mobileid.temp.output.name'), $bdocname) . '.asice');

							$response = new SignResponse();
							$response->status = 'success';
							$response->signedDocInfo = $Container->getDigiDoc();

							$Container->closeContainer();


							unlink(Flask()->Config->getTmpPath() . '/' . oneof(Flask()->Session->get('mobileid.temp.output.name'), $bdocname) . '.asice');

							Flask()->Session->set('mobileid.temp.output.file', '');
							Flask()->Session->set('mobileid.temp.output.name', '');


							break;
						}
						elseif (strpos($line, '[error]') !== false || strpos($line, 'Exception in thread "main"') !== false)
						{

							$line = substr($line, 0, strpos($line, ":"));
							$exception = trim(substr($line, strrpos($line, '.') + 1));
							error_log('error ' . $line);

							// Delete temp file
							unlink($filename);
							unlink(Flask()->Config->getTmpPath() . '/' . oneof(Flask()->Session->get('mobileid.temp.output.name'), $bdocname) . '.asice');

							Flask()->Session->set('mobileid.temp.output.file', '');
							Flask()->Session->set('mobileid.temp.output.name', '');

							switch ($exception)
							{
								case 'MidDeliveryException':
									throw new MidDeliveryException($line);
								case 'MidException':
									throw new SignException($line);
								case 'MidInternalErrorException':
									throw new MidInternalErrorException($line);
								case 'MidInvalidNationalIdentityNumberException':
									throw new MidInvalidNationalIdentityNumberException($line);
								case 'MidInvalidPhoneNumberException':
									throw new MidInvalidPhoneNumberException($line);
								case 'MidInvalidUserConfigurationException':
									throw new MidInvalidUserConfigurationException($line);
								case 'MidMissingOrInvalidParameterException':
									throw new MissingOrInvalidParameterException($line);
								case 'MidNotMidClientException':
									throw new MidNotMidClientException($line);
								case 'MidPhoneNotAvailableException':
									throw new MidPhoneNotAvailableException($line);
								case 'MidSessionNotFoundException':
									throw new MidSessionNotFoundException($line);
								case 'MidSessionTimeoutException':
									throw new MidSessionTimeoutException($line);
								case 'MidUnauthorizedException':
									throw new MidUnauthorizedException($line);
								case 'MidUserCancellationException':
									throw new MidUserCancellationException($line);
								default:
									throw new SignException($line);
							}

							break;
						}
						else
						{
							$response = new SignResponse();
							$response->status = 'pending';
						}
					}
					fclose($file);
				}

				return $response;

			}
			catch (MidDeliveryException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.DeliveryException]]');
			}
			catch (MidInternalErrorException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidInternalErrorException]]');
			}
			catch (MidInvalidNationalIdentityNumberException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidNationalIdentityNumberException]]');
			}
			catch (MidInvalidPhoneNumberException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidPhoneNumberException]]');
			}
			catch (MidInvalidUserConfigurationException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidUserConfigurationException]]');
			}
			catch (MissingOrInvalidParameterException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MissingOrInvalidParameterException]]');
			}
			catch (MidNotMidClientException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.NotMidClientException]]');
			}
			catch (MidPhoneNotAvailableException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.PhoneNotAvailableException]]');
			}
			catch (MidSessionNotFoundException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidSessionNotFoundException]]');
			}
			catch (MidSessionTimeoutException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidSessionTimeoutException]]');
			}
			catch (MidUnauthorizedException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.UnauthorizedException]]');
			}
			catch (MidUserCancellationException $e)
			{
				throw new SignException('[[FLASK.IDENTITY.MobileID.Error.UserCancellationException]]');
			}
			catch (\Exception $e)
			{
				switch ($e->getMessage())
				{
					case 'MidDeliveryException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.DeliveryException]]');
					case 'MidException':
					case 'MidInternalErrorException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidInternalErrorException]]');
					case 'MidInvalidNationalIdentityNumberException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidNationalIdentityNumberException]]');
					case 'MidInvalidPhoneNumberException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidPhoneNumberException]]');
					case 'MidInvalidUserConfigurationException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.InvalidUserConfigurationException]]');
					case 'MidMissingOrInvalidParameterException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MissingOrInvalidParameterException]]');
					case 'MidNotMidClientException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.NotMidClientException]]');
					case 'MidPhoneNotAvailableException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.PhoneNotAvailableException]]');
					case 'MidSessionNotFoundException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidSessionNotFoundException]]');
					case 'MidSessionTimeoutException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MidSessionTimeoutException]]');
					case 'MidUnauthorizedException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.UnauthorizedException]]');
					case 'MidUserCancellationException':
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.UserCancellationException]]');
					default:
						throw new SignException('[[FLASK.IDENTITY.MobileID.Error.MobileId.Other]]' . $e->getMessage());
				}
			}
		}

	}


?>